import React, { Component } from 'react'
import PropTypes from 'prop-types'
import  './index.scss'
import classNames from 'classnames'

class Super extends Component {
  render () {
    return (
      <div>
        <p
          className='super'
          onClick={() => this.props.actions.ADD_MESSAGE(this.props.addmessage)}>
          Introducing
        </p>
        <img className='image' src='https://res.cloudinary.com/swiggy/image/upload/q_auto/listing_menu_swiggy_assured_ob4sqt.webp' />
        <button className='button'><a href="swiggy://superPlan" target="_blank">Get Super Now!!</a></button>
      </div>
    )
  }
}

Super.propTypes = {
  actions: PropTypes.object.isRequired,
  addmessage: PropTypes.object
}

export default Super
